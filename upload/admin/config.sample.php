<?php
// HTTP
define('HTTP_SERVER', 'http://kamjiro.com/admin/');
define('HTTP_CATALOG', 'http://kamjiro.com/');

// HTTPS
define('HTTPS_SERVER', 'http://kamjiro.com/admin/');
define('HTTPS_CATALOG', 'http://kamjiro.com/');

// DIR
define('DIR_APPLICATION', '/var/www/html/kamjiro.com/current/upload/admin/');
define('DIR_SYSTEM', '/var/www/html/kamjiro.com/current/upload/system/');
define('DIR_LANGUAGE', '/var/www/html/kamjiro.com/current/upload/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/kamjiro.com/current/upload/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/kamjiro.com/current/upload/system/config/');
define('DIR_IMAGE', '/var/www/html/kamjiro.com/current/upload/image/');
define('DIR_CACHE', '/var/www/html/kamjiro.com/current/upload/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/html/kamjiro.com/current/upload/system/storage/download/');
define('DIR_LOGS', '/var/www/html/kamjiro.com/current/upload/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/html/kamjiro.com/current/upload/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/html/kamjiro.com/current/upload/system/storage/upload/');
define('DIR_CATALOG', '/var/www/html/kamjiro.com/current/upload/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'omokhudu1987');
define('DB_DATABASE', 'kamjiro');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
